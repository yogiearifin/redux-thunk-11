import {
  GET_DATA_USER_BEGIN,
  GET_DATA_USER_SUCCESS,
  GET_DATA_USER_FAIL,
} from "../actions/actionTypes";

const initialState = {
  loading: false,
  error: null,
  users: [],
  todos:[]
};

const userData = (state = initialState, action) => {
  const { type, payload, error } = action;
  switch (type) {
    case GET_DATA_USER_BEGIN:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case GET_DATA_USER_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
        users: payload,
      };
    case GET_DATA_USER_FAIL:
      return {
        ...state,
        loading: false,
        error: error,
        users: [],
      };
    default:
      return {
        ...state,
      };
  }
};

export default userData;
