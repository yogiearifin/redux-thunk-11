import {
  GET_DATA_HERO_BEGIN,
  GET_DATA_HERO_SUCCESS,
  GET_DATA_HERO_FAIL,
} from "../actions/actionTypes";

const initialState = {
  loading: false,
  error: null,
  hero: [],
};

const heroData = (state = initialState, action) => {
  const { type, payload, error } = action;
  switch (type) {
    case GET_DATA_HERO_BEGIN:
      return {
        ...state,
        loading: true,
      };
    case GET_DATA_HERO_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
        hero: payload,
      };
    case GET_DATA_HERO_FAIL:
      return {
        ...state,
        loading: false,
        error: error,
        hero: [],
      };
    default:
      return {
        ...state,
      };
  }
};

export default heroData;
