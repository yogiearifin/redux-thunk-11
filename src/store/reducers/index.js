import { combineReducers } from "redux";
import userData from "./userData";
import heroData from "./heroData";

const rootReducers = combineReducers({
  userData,
  heroData,
});

export default rootReducers;
