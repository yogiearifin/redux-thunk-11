export const GET_DATA_USER_BEGIN = "GET_DATA_USER_BEGIN ";
export const GET_DATA_USER_SUCCESS = "GET_DATA_USER_SUCCESS ";
export const GET_DATA_USER_FAIL = "GET_DATA_USER_FAIL ";

export const GET_DATA_HERO_BEGIN = "GET_DATA_HERO_BEGIN";
export const GET_DATA_HERO_SUCCESS = "GET_DATA_HERO_SUCCESS";
export const GET_DATA_HERO_FAIL = "GET_DATA_HERO_FAIL";
