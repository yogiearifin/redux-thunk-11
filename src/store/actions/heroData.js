import {
  GET_DATA_HERO_BEGIN,
  GET_DATA_HERO_SUCCESS,
  GET_DATA_HERO_FAIL,
} from "./actionTypes";

const token = "10218858437718605";

export const getDataHero = (names) => (dispatch) => {
  dispatch({
    type: GET_DATA_HERO_BEGIN,
    loading: true,
    error: null,
  });

  fetch(`https://superheroapi.com/api/${token}/search/${names}`, {
    method: "GET",
    header: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
  })
    .then((res) =>
      dispatch({
        type: GET_DATA_HERO_SUCCESS,
        loading: false,
        error: null,
        payload: res,
      })
    )
    .catch((err) =>
      dispatch({
        type: GET_DATA_HERO_FAIL,
        loading: false,
        error: err,
      })
    );
};
