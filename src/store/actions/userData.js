import {
  GET_DATA_USER_BEGIN,
  GET_DATA_USER_SUCCESS,
  GET_DATA_USER_FAIL,
} from "./actionTypes";

export const getDataUser = () => (dispatch) => {
  dispatch({
    type: GET_DATA_USER_BEGIN,
    loading: true,
    error: null,
  });
  fetch("https://jsonplaceholder.typicode.com/users")
    .then((data) => data.json())
    .then((res) =>
      dispatch({
        type: GET_DATA_USER_SUCCESS,
        loading: false,
        payload: res,
        error: null,
      })
    )
    .catch((err) =>
      dispatch({
        type: GET_DATA_USER_FAIL,
        loading: false,
        error: err,
      })
    );
};
