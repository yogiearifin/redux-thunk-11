import React, { useEffect, useState } from "react";
import { getDataUser } from "../store/actions/userData";
import { useSelector, useDispatch } from "react-redux";
import { getDataHero } from "../store/actions/heroData";

const UserData = () => {
  const dispatch = useDispatch();
  const dataUser = useSelector((state) => state.userData.users);
  const loading = useSelector((load) => load.userData.loading);
  const [stateHero, setStateHero] = useState();
  const onInput = (e) => {
    setStateHero(e.target.value);
  };

  const searchHero = () => {
    dispatch(getDataHero(stateHero));
  };
  useEffect(() => {
    dispatch(getDataUser());
  }, []);
  return (
    <div>
      <h1>User Data</h1>
      <h2>List of User</h2>
      {loading
        ? "Loading..."
        : dataUser &&
          dataUser.map((user, index) => {
            return (
              <div key={index}>
                <p>name:{user.name}</p>
                <p>email:{user.email}</p>
              </div>
            );
          })}
      {/* <input placeholder="input hero name" name="hero" onChange={onInput} />
      <button onClick={searchHero}>Search</button> */}
    </div>
  );
};

export default UserData;
