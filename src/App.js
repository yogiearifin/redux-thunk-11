import React from "react";
import { Provider } from "react-redux";
import store from "./store";
import UserData from "./components/UserData";

function App() {
  return (
    <Provider store={store}>
      <div>
        <h1>Redux</h1>
        <UserData />
      </div>
    </Provider>
  );
}

export default App;
